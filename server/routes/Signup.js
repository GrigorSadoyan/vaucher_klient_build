var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");
var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');


router.get('/',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});


router.post('/', function(req, res, next) {
    let reqQuery = {
        f_name : req.body.f_name,
        l_name : req.body.l_name,
        phone : req.body.phone,
        password : req.body.password
    };

    validParams(reqQuery,req.cookies)
        .then(function (resultParams) {
            if (resultParams.redirect) {
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/sign-up",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:"10487",data:null});
            } else {
                onSignUp(resultParams)
                    .then(function (result) {
                        if(!result.data.error) {
                            res.json({error: false, msg: "oks"});
                            res.end()
                        }else{
                            res.json({error: true, msg: result.data.msg});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:error.toString(),route:"/sign-up",requestType:"POST",function:"axios",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"8461515615",data:null});
                        res.end()
                    })
            }
        })
})


router.post('/code', function(req, res, next) {
    var phoneCode = req.body.code;
    validParamsPhoneCode(phoneCode)
        .then(function (resultParams) {
            if (resultParams.redirect) {
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/sign-up/code",requestType:"POST",function:"validParamsPhoneCode",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:resultParams.msg,data:null});
            } else {
                console.log('resultParams XXX',resultParams)
                onSendPhoneFourDigit(resultParams)
                    .then(function (result) {
                        if(!result.data.error) {
                            res.cookie("vvach", result.data.data[0].token,{maxAge:360000000000*24*31});
                            res.json({error: false, msg: 'ok', data: result.data});
                            res.end()
                        }else{
                            res.json({error: true, msg: result.data.msg});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:error.toString(),route:"/sign-up",requestType:"POST",function:"axios",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"2541",data:null});
                        res.end()
                    })
            }
        })
})


router.post('/resend', function(req, res, next) {
    console.log('...........',req.body)
    var phone = req.body.phone;
        onStartResending(phone)
            .then(function (result) {
                console.log('result XXX ',result)
                if(!result.data.error) {
                    res.json({error: false, msg: 'ok'});
                    res.end()
                }else{
                    res.json({error: true, msg: result.data.msg});
                    res.end()
                }
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify({error:error.toString(),route:"/sign-up",requestType:"POST",function:"axios",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:"2541",data:null});
                res.end()
            })
})

module.exports = router;

function onSignUp(data){
    let url = `${API_URL}/api/v1/sign-up`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:data
    }
    console.log('sendrequest',sendrequest)
    return axios(sendrequest);
}

function validParams(reqQuery,reqCookie) {
    return new Promise(function (resolve, reject) {


        let reslovObj = {
            redirect:false,
            f_name : null,
            l_name : null,
            phone : reqQuery.phone,
            password : null,
        };

        let namesTest = /[~!@#$?.%^&*()_+/|,]/g;
        if(typeof reqQuery.f_name == "undefined" || reqQuery.f_name.match(namesTest) || reqQuery.f_name.length == 0){
            resolve({redirect: true, msg:"f_name"});
        }else{
            reslovObj.f_name = reqQuery.f_name;
        }
        if(typeof reqQuery.l_name == "undefined" || reqQuery.l_name.match(namesTest) || reqQuery.l_name.length == 0){
            resolve({redirect: true, msg:"l_name"});
        }else{
            reslovObj.l_name = reqQuery.l_name;
        }

        if(typeof reqQuery.password == "undefined" || reqQuery.password.length < 7) {
            resolve({redirect: true, msg:"password length"});
        }else if(reqQuery.password.length == 0){
            resolve({redirect: true, msg:"password empty"});
        }else{
            reslovObj.password = reqQuery.password;
        }

        resolve(reslovObj);
    })
}
function validParamsPhoneCode(phoneCode,reqCookie){
    return new Promise(function (resolve, reject) {


        let reslovObj = {
            redirect:false,
            // token:null,
            phoneCode:null
        };
        // if(typeof reqCookie.vvach == "undefined"){
        //     resolve({redirect: true, msg:"vvach"});
        // }else{
        //     reslovObj.token = reqCookie.vvach;
        // }

        if(typeof phoneCode == "undefined" || phoneCode.length != 4){
            resolve({redirect: true, msg:"phoneCode"});
        }else{
            reslovObj.phoneCode = phoneCode;
        }

        resolve(reslovObj);
    })
}

function onSendPhoneFourDigit(data,reqCookie){
    let url = `${API_URL}/api/v1/sign-up/code`;
    var params  = {
        code:data.phoneCode
    }
    var sendrequest = {
        method: 'POST',
        headers: {
            "authorization": AUTH_API,
            // "void": data.token,
        },
        url:url,
        data:params
    }
    return axios(sendrequest);
}

function onStartResending(phone,reqCookie){
    let url = `${API_URL}/api/v1/sign-up/resend`;
    var sendrequest = {
        method: 'POST',
        headers: {
            "authorization": AUTH_API,
            // "void": data.token,
        },
        url:url,
        data:{phone:phone}
    }
    console.log('sendrequest',sendrequest)
    return axios(sendrequest);
}
