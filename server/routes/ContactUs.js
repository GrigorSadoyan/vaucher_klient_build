var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");

var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');

router.get('/',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});

router.post('/add', function(req, res, next) {
    let reqQuery = {
        f_name : req.body.f_name,
        l_name : req.body.l_name,
        phone : req.body.phone,
        description : req.body.description,
    }
    console.log('reqQuery',reqQuery)

    validParams(reqQuery)
        .then(function (data) {
            onSend(data)
                .then(function () {
                    res.json({error: false, msg: 'ok'});
                    res.end()
                })
                .catch(function () {
                    res.json({error: true, msg: '5876764864864'});
                    res.end()
                })
        })
        .catch(function () {
            res.json({error: true, msg: '4356786545355'});
            res.end()
        })
});

module.exports = router;

function onSend(data){
    let url = `${API_URL}/api/v1/contact-us/add`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:{
            "f_name":data.f_name,
            "l_name":data.l_name,
            "description":data.description,
            "phone":data.phone,
        }
    };

    return axios(sendrequest);
}

function validParams(reqQuery) {
    return new Promise(function (resolve, reject) {

        var f_name = null;
        var l_name = null;
        var description = null;
        var phone = null;


        if (typeof reqQuery.f_name != "undefined" && reqQuery.f_name != "") {
            f_name = reqQuery.f_name
        }else{
            resolve({redirect: true, msg:"phone"});
        }

        if (typeof reqQuery.l_name != "undefined" && reqQuery.l_name != "") {
            l_name = reqQuery.l_name
        }else{
            l_name = ''
        }

        if (typeof reqQuery.description != "undefined" && reqQuery.description != "") {
            description = reqQuery.description
        }else{
            resolve({redirect: true, msg:"phone"});
        }

        if (typeof reqQuery.phone != "undefined" && reqQuery.phone != "") {
            phone = reqQuery.phone
        }else{
            resolve({redirect: true, msg:"phone"});
        }


        resolve({
            redirect: false,
            phone: phone,
            f_name: f_name,
            l_name: l_name,
            description: description,
        });
    })
}
