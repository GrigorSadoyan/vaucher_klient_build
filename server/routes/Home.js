var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');

router.get('/',middleware.staticStore, function(req, res, next) {
    // var random = Math.floor(Math.random()*99).toString();
    console.log("home router");
    createContent.sendContent(req, res);

})

module.exports = router;