var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");

var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');

router.get('/',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});

module.exports = router;