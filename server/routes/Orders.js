var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");
var {API_URL,AUTH_API,SITE_URL} = require('../static');
var axios = require('axios');
var ZarinpalCheckout = require('zarinpal-checkout');
var zarinpal = ZarinpalCheckout.create('198bc4a2-543c-11e9-aec8-000c295eb8fc', false);


router.post('/flipmoney/buy',middleware.staticStore, function(req, res, next) {
    if(typeof req.cookies.vvach !== 'undefined'){
        createProductFlipMoney(req.cookies,req.body)
            .then(function (result) {
                if (!result.data.error) {
                    console.log('result.data 1111',result.data)
                    // res.json({error: false, msg: "ok",data:result.data.data});
                    var orderId = result.data.data.insertId;
                    // console.log('result.data.data.insertId',result.data.data.insertId)
                    // console.log('result.data.data.total',result.data.data.total)
                    var amount = result.data.data.total;
                    // var amount = 500;
                    var back = SITE_URL+'/orders/done/'+orderId+'/'+amount+'';
                    zarinpal.PaymentRequest({
                        Amount: amount,
                        CallbackURL: back,
                        Description: 'GIFT card',
                        Email: 'sadoyangrigor@gmail.com',
                        Mobile: '09120000000'
                    }).then(function (response) {
                        if (response.status == 100) {
                            console.log('response.status',response.status)
                            res.redirect(response.url);
                        }
                    }).catch(function (err) {
                        console.log(err);
                    });
                    // res.end()
                } else {
                    res.redirect("/sign-in");
                }
            })
            .catch(function () {

            })
    }else{
        res.redirect("/sign-in");
    }
})


router.post('/create',middleware.staticStore, function(req, res, next) {
    orderCreate(req.cookies)
        .then(function (result) {
            if (!result.data.error) {
                // console.log('result.data 1111',result.data)
                // res.json({error: false, msg: "ok",data:result.data.data});
                var orderId = result.data.data.insertId;
                // console.log('result.data.data.insertId',result.data.data.insertId)
                // console.log('result.data.data.total',result.data.data.total)
                //var amount = result.data.data.total;
                var amount = 500;
                var back = SITE_URL+'/orders/done/'+orderId+'/'+amount+'';
                zarinpal.PaymentRequest({
                    Amount: amount,
                    CallbackURL: back,
                    Description: 'GIFT card',
                    Email: 'sadoyangrigor@gmail.com',
                    Mobile: '09120000000'
                }).then(function (response) {
                    if (response.status == 100) {
                        res.redirect(response.url);
                    }
                }).catch(function (err) {
                    console.log(err);
                });
                // res.end()
            } else {
                res.redirect("/sign-in");
            }

        })
        .catch(function (error) {
            console.log('ERORR 9999-9')
            res.json({error:true,msg:"9999-9"});
            res.end()
        })
})

router.get('/done/:orderId/:amount', function(req, res, next) {
    zarinpal.PaymentExtraVerification({
        Amount: req.params.amount, // In Tomans
        Authority: req.query.Authority,
    }).then(response => {
        if (response.status === -21) {
            res.json({Verified:"Empty"});
            res.end();
        } else {
            console.log('response',response)
            var results = {
                status:1,
                ref_id : response.RefID,
                cart : response.ExtraDetail.Transaction.CardPanMask,
                order_id:req.params.orderId
            }
            console.log('results',results);
            insResults(req.cookies,results)
                .then(function () {
                    console.log('red',true);
                    res.redirect("/profile/orders/success");
                })
                .catch(function () {
                    console.log('red',false);
                    res.redirect("/profile/orders/failed");
                })

            // res.json({res:response});
            // res.end();
        }
    }).catch(err => {
        console.error(err);
    });
});

module.exports = router;



function orderCreate(reqCookie){

    var token = "";
    var tokenType = "";
    if(typeof reqCookie.gvach != "undefined"){
        token = reqCookie.gvach;
        tokenType = "guest";
    }

    if(typeof reqCookie.vvach != "undefined"){
        token = reqCookie.vvach;
        tokenType = "user";
    }

    let header = {
        "authorization": AUTH_API
    };
    if(tokenType == 'user'){
        header['void'] = token;
    }else{
        header['guest'] = token;
    }

    let url = `${API_URL}/api/v1/orders/create`
    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:{}
    }
    return axios(sendrequest);
}

function insResults(reqCookie,results){

    var token = "";
    var tokenType = "";
    if(typeof reqCookie.gvach != "undefined"){
        token = reqCookie.gvach;
        tokenType = "guest";
    }

    if(typeof reqCookie.vvach != "undefined"){
        token = reqCookie.vvach;
        tokenType = "user";
    }

    let header = {
        "authorization": AUTH_API
    };
    if(tokenType == 'user'){
        header['void'] = token;
    }else{
        header['guest'] = token;
    }

    let url = `${API_URL}/api/v1/orders/success`
    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:{results}
    }
    return axios(sendrequest);
}
function createProductFlipMoney(reqCookie,params){

    var token = "";

    if(typeof reqCookie.vvach != "undefined"){
        token = reqCookie.vvach;
    }

    let header = {
        "authorization": AUTH_API,
        "void" : token
    };

    let url = `${API_URL}/api/v1/orders/flipmoney/add`
    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:{params}
    }
    return axios(sendrequest);
}