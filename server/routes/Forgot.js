var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");

var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');

router.get('/',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});

router.post('/phone', function(req, res, next) {
    var reqQuery = {
        phone : req.body.phone
    }

    validParams(reqQuery)
        .then(function (data) {
            onSendForgot(data)
                .then(function (result) {
                    console.log('result',result)
                    if(!result.data.error){
                        res.json({error: false, msg: 'ok',data : result.data});
                        res.end()
                    }else {
                        res.json({error: true, msg: '1534864165135'});
                        res.end()
                    }
                })
                .catch(function () {
                    res.json({error: true, msg: '5876764864864'});
                    res.end()
                })
        })
        .catch(function () {
            res.json({error: true, msg: '4356786545355'});
            res.end()
        })
});

router.post('/phone/code', function(req, res, next) {
    var reqQuery = {
        code : req.body.code
    }

    validParamsCode(reqQuery)
        .then(function (data) {
            onSendForgotCode(data)
                .then(function (result) {
                    console.log('result',result)
                    if(!result.data.error){
                        res.json({error: false, msg: 'ok',data : result.data});
                        res.end()
                    }else {
                        res.json({error: true, msg: '1534864165135'});
                        res.end()
                    }
                })
                .catch(function () {
                    res.json({error: true, msg: '5876764864864'});
                    res.end()
                })
        })
        .catch(function () {
            res.json({error: true, msg: '4356786545355'});
            res.end()
        })
});

router.post('/phone/password/recovery', function(req, res, next) {
    var reqQuery = {
        pass : req.body.pass,
        repass : req.body.repass,
        toks : req.body.toks,
    }

    validParamsRecovery(reqQuery)
        .then(function (data) {
            onSendVerification(data)
                .then(function (result) {
                    console.log('result',result)
                    if(!result.data.error){
                        console.log('result.data',result.data)
                        result.data.data[0]['isAuthenticated'] = true;
                        res.cookie("vvach", result.data.data[0]['token'],{maxAge:3600000*24*31});
                        res.json({error: false, msg: 'ok',data : result.data});
                        res.end()
                    }else {
                        res.json({error: true, msg: '1534864165135'});
                        res.end()
                    }
                })
                .catch(function () {
                    res.json({error: true, msg: '5876764864864'});
                    res.end()
                })
        })
        .catch(function () {
            res.json({error: true, msg: '4356786545355'});
            res.end()
        })
});

module.exports = router;



function validParams(reqQuery) {
    return new Promise(function (resolve, reject) {

        var phone = null;
        if (typeof reqQuery.phone != "undefined" && reqQuery.phone != "") {
            phone = reqQuery.phone
        }else{
            resolve({redirect: true, msg:"phone"});
        }

        resolve({
            redirect: false,
            phone: phone,
        });
    })
}


function validParamsCode(reqQuery) {
    return new Promise(function (resolve, reject) {

        var code = null;
        if (typeof reqQuery.code != "undefined" && reqQuery.code != "") {
            code = reqQuery.code
        }else{
            resolve({redirect: true, msg:"code"});
        }

        resolve({
            redirect: false,
            code: code,
        });
    })
}


function validParamsRecovery(reqQuery) {
    return new Promise(function (resolve, reject) {

        var pass = null;
        var repass = null;
        var toks = null;


        if (typeof reqQuery.pass != "undefined" && reqQuery.pass != "") {
            pass = reqQuery.pass
        }else{
            resolve({redirect: true, msg:"code"});
        }
        if (typeof reqQuery.repass != "undefined" && reqQuery.pass != "") {
            repass = reqQuery.repass
        }else{
            resolve({redirect: true, msg:"repassrepass"});
        }
        if (typeof reqQuery.toks != "undefined" && reqQuery.toks != "") {
            toks = reqQuery.toks
        }else{
            resolve({redirect: true, msg:"toks"});
        }

        resolve({
            redirect: false,
            params : {
                pass: pass,
                toks: toks,
                repass: repass
            }
        });
    })
}


function onSendForgot(data){
    let url = `${API_URL}/api/v1/forgot/phone`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:{
            "phone":data.phone,
        }
    };

    return axios(sendrequest);
}


function onSendForgotCode(data){
    let url = `${API_URL}/api/v1/forgot/phone/code`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:{
            "code":data.code,
        }
    };

    return axios(sendrequest);
}

function onSendVerification(data){
    let url = `${API_URL}/api/v1/forgot/password/new`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:{
            "pass":data.params.pass,
            "repass":data.params.repass,
            "toks":data.params.toks
        }
    };
    console.log('sendrequest',sendrequest)
    return axios(sendrequest);
}