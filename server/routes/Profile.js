var express = require('express');
var router = express.Router();
var log = require("../logs/log");
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var multer  = require('multer');
var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');
const fs = require('fs');
var upload = multer();

upload.storage = multer.diskStorage({
    destination: './public/images/passport',
    filename: function (req, file, cb) {
        var type = file.mimetype.split("/");

        var random = Math.round(Math.random()*1299999);
        var fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});

router.get('/user',middleware.staticStore, function(req, res, next) {
    if(req.userAuth) {
        getPassports(req.cookies)
            .then(function (result) {
                console.log('...................',result.data)
                createContent.store.dispatch({type: 'ADD_PASSPORTS', payload: result.data.data});
                createContent.sendContent(req, res);
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify({error:error,route:"/user/passports/all",requestType:"GET",function:"getPassports",date:new Date()}),"./server/logs/logs.txt");
                createContent.store.dispatch({type: 'ADD_PASSPORTS', payload:[]});
                createContent.sendContent(req, res);
            })
    }else{
        res.redirect("/");
        res.end();
    }
})

router.post('/user/passports/all',middleware.staticStore, function(req, res, next) {
    if(req.userAuth) {
        getPassports(req.cookies)
            .then(function (result) {
                if(!result.data.error){
                    res.json({error: false, msg: "ok", data: result.data.data});
                    res.end()
                    // createContent.store.dispatch({type: 'ADD_ORDERS_SSR', payload: true});
                    // createContent.store.dispatch({type: 'ADD_PASSPORTS', payload: result.data.data});
                    // createContent.sendContent(req, res);
                }
            })
            .catch(function (error) {
                // log.insertLog(JSON.stringify({error:error,route:"/user/passports/all",requestType:"GET",function:"getPassports",date:new Date()}),"./server/logs/logs.txt");
                // createContent.store.dispatch({type: 'ADD_PASSPORTS', payload:[]});
                // createContent.sendContent(req, res);
                res.json({error: true, msg: "864351684135135"});
                res.end()
            })
    }else{
        res.redirect("/");
        res.end();
    }
})

router.post('/user/addpassport',upload.array('img', 12), function(req, res, next) {
    let reqQuery = {
        bankCardNumber : req.body.bank,
        img : req.files[0].filename
    };
    console.log('reqQuery',reqQuery)
    validParams(reqQuery)
        .then(function (resultParams) {
            console.log('resultParams',resultParams)
            if (resultParams.redirect) {
                console.log('ERROR 111111111')
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/profile/user/addpassport",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:"2147112",msgText:resultParams.msg,data:null});
            } else {
                addUserPassport(resultParams,req.cookies)
                    .then(function (result) {
                        console.log('result',result)
                        if(!result.data.error) {
                            var img = result.data.frontImg;
                            fs.unlink('./public/images/passport/'+img+'', (err) => {
                                if (err) throw err;
                            });
                            res.json({error: false, msg: "ok", data: result.data});
                            res.end()
                        }else{
                            res.json({error: true, msg: "9999",msgText:"response error", data: null});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:resultParams.msg,route:"/add",requestType:"POST",function:"getProduct",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"ok",msgText:resultParams.msg,data:null});
                        res.end()
                    })
            }
        })
        .catch(function () {
            res.json({error:true,msg:"32467657855345"});
            res.end()
        })
});


router.post('/orders/all',middleware.staticStore, function(req, res, next) {
    if(req.userAuth) {
        getOrders(req.cookies)
            .then(function (result) {
                res.json({error: false, msg: "ok", data: result.data});
                res.end()
            })
            .catch(function () {
                res.json({error: true, data: []});
                res.end()
            })
    }
});


router.get('/orders',middleware.staticStore, function(req, res, next) {
    if(req.userAuth) {
        getOrders(req.cookies)
            .then(function (result) {
                console.log('XXXXXXXXXXXX',result.data)
                createContent.store.dispatch({type: 'ADD_ORDERS', payload: result.data.data});
                createContent.sendContent(req, res);
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify({error:error,route:"/user/passports/all",requestType:"GET",function:"getPassports",date:new Date()}),"./server/logs/logs.txt");
                createContent.store.dispatch({type: 'ADD_ORDERS', payload:[]});
                createContent.sendContent(req, res);
            })
    }
});

router.get('/orders/success',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});

router.get('/orders/failed',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
});

module.exports = router;


function getPassports(reqCookie){

    let url = `${API_URL}/api/v1/profile/user/passport/all`;

    let header = {
        "authorization": AUTH_API,
        "void" : reqCookie.vvach
    };

    var sendrequest = {
        method: 'GET',
        headers: header,
        url:url,
    }
    return axios(sendrequest);
}

function getOrders(reqCookie){
    if(typeof reqCookie.vvach === "undefined"){
        return false;
    }
    let url = `${API_URL}/api/v1/orders/all`;

    let header = {
        "authorization": AUTH_API,
        "void" : reqCookie.vvach
    };


    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
    }
    // console.log('sendrequest',sendrequest)
    return axios(sendrequest);
}

function addUserPassport(resultParams,reqCookie){

    let url = `${API_URL}/api/v1/profile/user/addpassport`;
    let sendParam = {
        "bankCardNumber": resultParams.info.bankCardNumber,
        "img": resultParams.info.img,
    };
    let header = {
        "authorization": AUTH_API,
        "void" : reqCookie.vvach
    };

    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:sendParam
    }
    return axios(sendrequest);
}


function validParams(reqQuery) {
    return new Promise(function (resolve, reject) {
        var info = {
            bankCardNumber:null,
            img:null,
        }

        if (typeof reqQuery.bankCardNumber != "undefined" && reqQuery.bankCardNumber != "") {
            info.bankCardNumber = reqQuery.bankCardNumber
        }else{
            resolve({redirect: true, msg:"bankCardNumber"});
        }

        if (typeof reqQuery.img != "undefined" && reqQuery.img != "") {
            info.img = reqQuery.img
        }else{
            resolve({redirect: true, msg:"img"});
        }

        // console.log('info.bankCardNumber',info.bankCardNumber)
        // console.log('info.img',info.img)
        resolve({
            redirect: false,
            info:info
        });
    })
}
