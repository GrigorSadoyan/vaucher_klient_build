var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");

var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');

router.get('/',middleware.staticStore, function(req, res, next) {
    // cart(req.cookies)
    //     .then(function (result) {
    //         createContent.store.dispatch({type: 'ADD_CART_SSR', payload: true});
    //         if(result.data.data.cart.length > 0) {
    //             if(!result.data.data.error) {
    //                 createContent.store.dispatch({type: 'ADD_CART', payload: result.data.data.cart});
                    createContent.sendContent(req, res);
        //         }else{
        //             createContent.store.dispatch({type: 'ADD_CART', payload: []});
        //             createContent.sendContent(req, res);
        //         }
        //     }else{
        //         createContent.store.dispatch({type: 'ADD_CART', payload: []});
        //         createContent.sendContent(req, res);
        //     }
        // })
        // .catch(function (error) {
        //     log.insertLog(JSON.stringify({error:error,route:"/cart",requestType:"GET",function:"cart",date:new Date()}),"./server/logs/logs.txt");
        //     createContent.store.dispatch({type: 'ADD_CART', payload: []});
        //     createContent.sendContent(req, res);
        // })
})

router.get('/all',middleware.staticStore, function(req, res, next) {
    getCart(req.cookies)
        .then(function (result) {
            if(result.data.data.cart.length > 0) {
                if(!result.data.data.error) {
                    res.json({error:false,msg:"ok",
                        data:{
                            cart:result.data.data.cart
                        }
                    });
                    res.end()
                }else{
                    res.json({error:true,msg:"ok",
                        data:{
                            cart:[]
                        }
                    });
                    res.end()
                }
            }else{
                res.json({error:true,msg:"9999",
                    data:{
                        cart:[]
                    }
                });
                res.end()
            }
        })
        .catch(function (error) {
            res.json({error:true,msg:"9999-9",
                data:{
                    cart:[]
                }
            });
            res.end()
        })
})

router.post('/add', function(req, res, next) {
    let reqQuery = {
        prodId : req.body.params.prodId,
        count : req.body.params.count
    }
    console.log('reqQuery',reqQuery)
    validParams(reqQuery,req.cookies)
        .then(function (resultParams) {
            if (resultParams.redirect) {
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/add",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:"2147112",msgText:resultParams.msg,data:null});
            } else {
                console.log('resultParams',resultParams)
                addToCart(resultParams)
                    .then(function (result) {
                        if(!result.data.error) {
                            res.json({error: false, msg: "ok", data: result.data.data.cart});
                            res.end()
                        }else{
                            res.json({error: true, msg: "9999",msgText:"response error", data: null});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:resultParams.msg,route:"/add",requestType:"POST",function:"getProduct",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"ok",msgText:resultParams.msg,data:null});
                        res.end()
                    })
            }
        })
        .catch(function () {
            res.json({error:true,msg:"32467657855345"});
            res.end()
        })
})

router.post('/remove',function(req, res, next) {
    validParamsDelete(req.body.prodId,req.cookies)
        .then(function (resultParams) {
            if (resultParams.redirect) {
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/remove",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:true,msg:"464343253454353",msgText:resultParams.msg,data:null});
            } else {
                removeFromCart(resultParams)
                    .then(function (result) {
                        if(!result.data.error) {
                            res.json({error:false,msg:"ok",data:null});
                            res.end()
                        }else{
                            res.json({error:true,msg:"ok",data:null});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:resultParams.msg,route:"/remove",requestType:"POST",function:"removeFromCart",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"ok",msgText:resultParams.msg,data:null});
                        res.end()
                    })
            }
        })
})

// router.post('/count',function(req, res, next) {
//     let reqQuery = {
//         prodId : req.body.params.prodId,
//         count : 1,
//         actionType:req.body.params.actionType
//     }
//
//     validParamsCount(reqQuery,req.cookies)
//         .then(function (resultParams) {
//             if (resultParams.redirect) {
//                 log.insertLog(JSON.stringify({error:resultParams.msg,route:"/count",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
//                 res.json({error:true,msg:"78687557658",msgText:resultParams.msg,data:null});
//             } else {
//                 changeCount(resultParams)
//                     .then(function (result) {
//                         console.log('result',result)
//                         if(!result.data.error) {
//                             res.json({ error: false, msg: "ok", data: null});
//                             res.end()
//                         }else{
//                             res.json({error: true, msg: "ok",msgText:"response error", data: null});
//                             res.end()
//                         }
//                     })
//                     .catch(function (error) {
//                         log.insertLog(JSON.stringify({error:resultParams.msg,route:"/count",requestType:"POST",function:"getProduct",date:new Date()}),"./server/logs/logs.txt");
//                         res.json({error:true,msg:"ok",msgText:resultParams.msg,data:null});
//                         res.end()
//                     })
//             }
//         })
// })

module.exports = router;

function getCart(reqCookie){

    var lang = LANGUAGE;
    if(typeof reqCookie.language != "undefined"){
        switch (reqCookie.language) {
            case "ru":
                lang = 2;
                break;
            case "en":
                lang = 1;
                break;
            case "uk":
                lang = 3;
                break;
        }
    }

    var token = "";
    var tokenType = "";
    if(typeof reqCookie.gseven != "undefined"){
        token = reqCookie.gseven;
        tokenType = "guest";
    }

    if(typeof reqCookie.vseven != "undefined"){
        token = reqCookie.vseven;
        tokenType = "user";
    }

    let header = {
        "authorization": AUTH_API
    };
    if(tokenType == 'user'){
        header['void'] = token;
    }else{
        header['guest'] = token;
    }

    let url = `${API_URL}/api/v2/cart/all/${lang}/`
    var sendrequest = {
        method: 'GET',
        headers: header,
        url:url,
        data:{}
    }

    return axios(sendrequest);
}

function addToCart(params){
    let url = `${API_URL}/api/v1/cart/add`;
    let sendParam = {
        "count": params.count,
        "prodId": params.prodId,
    };
    let header = {
        "authorization": AUTH_API
    };
    if(params.tokenType == 'user'){
        header['void'] = params.token;
    }else{
        header['guest'] = params.token;
    }
    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:sendParam
    }
    console.log('sendrequest',sendrequest)
    return axios(sendrequest);
}

function removeFromCart(params){
    let url = `${API_URL}/api/v1/cart/remove`;
    let sendParam = {
        "prodId": params.prodId,
    };
    let header = {
        "authorization": AUTH_API
    };
    if(params.tokenType == 'user'){
        header['void'] = params.token;
    }else{
        header['guest'] = params.token;
    }
    var sendrequest = {
        method: 'POST',
        headers: header,
        url:url,
        data:sendParam
    }

    return axios(sendrequest);
}

// function changeCount(params){
//     let url = `${API_URL}/api/v1/cart/count`;
//     let sendParam = {
//         "count": params.count,
//         "prodId": params.prodId,
//         "actionType": params.actionType,
//     };
//     let header = {
//         "authorization": AUTH_API
//     };
//     if(params.tokenType == 'user'){
//         header['void'] = params.token;
//     }else{
//         header['guest'] = params.token;
//     }
//     var sendrequest = {
//         method: 'POST',
//         headers: header,
//         url:url,
//         data:sendParam
//     }
//
//     return axios(sendrequest);
// }

function validParams(reqQuery,reqCookie) {
    return new Promise(function (resolve, reject) {

        var token = "";
        var tokenType = "";
        if(typeof reqCookie.gvach != "undefined"){
            token = reqCookie.gvach;
            tokenType = "guest";
        }

        if(typeof reqCookie.vvach != "undefined"){
            token = reqCookie.vvach;
            tokenType = "user";
        }

        var count = 1;
        var prodId = null;
        // var actionType = null;

        // if (typeof reqQuery.count != "undefined" && !isNaN(reqQuery.count)) {
        //     count = reqQuery.count
        // }else{
        //     resolve({redirect: true, msg:"count"});
        // }

        if (typeof reqQuery.prodId != "undefined" && !isNaN(reqQuery.prodId)) {
            prodId = reqQuery.prodId
        }else{
            resolve({redirect: true, msg:"prodId"});
        }

        // if (typeof reqQuery.actionType != "undefined" && !isNaN(reqQuery.actionType)) {
        //     actionType = reqQuery.actionType
        // }else{
        //     resolve({redirect: true, msg:"actionType"});
        // }

        resolve({
            redirect: false,
            count: count,
            prodId: prodId,
            // actionType: actionType,
            token:token,
            tokenType:tokenType
        });
    })
}
// function validParamsCount(reqQuery,reqCookie) {
//     return new Promise(function (resolve, reject) {
//
//         var token = "";
//         var tokenType = "";
//         if(typeof reqCookie.gvach != "undefined"){
//             token = reqCookie.gvach;
//             tokenType = "guest";
//         }
//
//         if(typeof reqCookie.vvach != "undefined"){
//             token = reqCookie.vvach;
//             tokenType = "user";
//         }
//
//         var count = null;
//         var prodId = null;
//         var actionType = null;
//
//         if (typeof reqQuery.count != "undefined" && !isNaN(reqQuery.count)) {
//             count = reqQuery.count
//         }else{
//             resolve({redirect: true, msg:"count"});
//         }
//
//         if (typeof reqQuery.prodId != "undefined" && !isNaN(reqQuery.prodId)) {
//             prodId = reqQuery.prodId
//         }else{
//             resolve({redirect: true, msg:"prodId"});
//         }
//         if (typeof reqQuery.actionType != "undefined") {
//             actionType = reqQuery.actionType
//         }else{
//             resolve({redirect: true, msg:"actionType"});
//         }
//
//         resolve({
//             redirect: false,
//             count: count,
//             prodId: prodId,
//             actionType: actionType,
//             token:token,
//             tokenType:tokenType
//         });
//     })
// }

function validParamsDelete(prodIds,reqCookie) {
    return new Promise(function (resolve, reject) {

        var token = "";
        var tokenType = "";
        if(typeof reqCookie.gvach != "undefined"){
            token = reqCookie.gvach;
            tokenType = "guest";
        }

        if(typeof reqCookie.vvach != "undefined"){
            token = reqCookie.vvach;
            tokenType = "user";
        }

        var prodId = null;

        if (typeof prodIds != "undefined" && !isNaN(prodIds)) {
            prodId = prodIds
        }else{
            resolve({redirect: true, msg:"prodId"});
        }

        resolve({
            redirect: false,
            prodId: prodId,
            token:token,
            tokenType:tokenType
        });
    })
}