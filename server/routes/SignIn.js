var express = require('express');
var router = express.Router();
var createContent = require('../createContent');
var middleware = require('../middleware/middleWare');
var log = require("../logs/log");
var {API_URL,AUTH_API,LANGUAGE} = require('../static');
var axios = require('axios');

router.get('/',middleware.staticStore, function(req, res, next) {
    createContent.sendContent(req, res);
})

router.post('/', function(req, res, next) {
    let reqQuery = {
        phone : req.body.phone,
        password : req.body.password,
        remember:req.body.active
    }

    validParams(reqQuery,req.cookies)
        .then(function (resultParams) {
            if (resultParams.redirect) {
                log.insertLog(JSON.stringify({error:resultParams.msg,route:"/sign-in",requestType:"POST",function:"validParams",date:new Date()}),"./server/logs/logs.txt");
                res.json({error:false,msg:"10487",data:null});
            } else {
                onLogin(resultParams)
                    .then(function (result) {
                        console.log('result',result)
                        if(!result.data.error) {
                            if (result.data.data != null) {
                                if (result.data.data.user.length > 0) {
                                    result.data.data.user[0]['isAuthenticated'] = true;
                                    res.cookie("vvach", result.data.data.user[0]['token'],{maxAge:3600000*24*31});
                                    res.json({
                                        error: false, msg: "ok",
                                        data: {
                                            user: result.data.data.user[0],
                                            cart: result.data.data.cart,
                                            // favoite:result.data.data.favorite,
                                            // lastvierw:result.data.data.lastview,

                                        }
                                    });
                                    res.end()
                                } else {
                                    res.json({error: true, msg: result.data.msg, data: null});
                                    res.end()
                                }
                            }else{
                                res.json({error: true, msg: '104', data: null});
                                res.end()
                            }
                        }else{
                            res.json({error: true, msg: result.data.msg, data: null});
                            res.end()
                        }
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify({error:error,route:"/sign-in",requestType:"POST",function:"axios",date:new Date()}),"./server/logs/logs.txt");
                        res.json({error:true,msg:"2541",data:null});
                        res.end()
                    })
            }
        })
})

module.exports = router;



function onLogin(data){
    let url = `${API_URL}/api/v1/sign-in`;
    var sendrequest = {
        method: 'POST',
        headers: { "authorization": AUTH_API },
        url:url,
        data:{
            "phone":data.phone,
            "password":data.password,
            "guest":data.guest,
        }
    };
    console.log('sendrequest',sendrequest);

    return axios(sendrequest);
}

function validParams(reqQuery,reqCookie) {
    return new Promise(function (resolve, reject) {

        var phone = null;
        var password = null;
        var guest = null;
        var remember = reqQuery.remember || false;

        if (typeof reqQuery.phone != "undefined" && reqQuery.phone != "") {
            phone = reqQuery.phone
        }else{
            resolve({redirect: true, msg:"phone"});
        }

        if (typeof reqQuery.password != "undefined" && reqQuery.password != "") {
            password = reqQuery.password
        }else{
            resolve({redirect: true, msg:"password"});
        }

        if(typeof reqCookie.gvach != "undefined"){
            guest = reqCookie.gvach;
        }

        resolve({
            redirect: false,
            phone: phone,
            password: password,
            remember: remember,
            guest:guest
        });
    })
}
