import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import { getBundles } from 'react-loadable/webpack'
import stats from '../react-loadable.json';
import Routes from '../src/router/Routes';

export default (pathname, store, context) => {

    console.log("render",pathname)
    let modules = [];
  const content = renderToString(
      <Loadable.Capture report={moduleName => modules.push(moduleName)}>
        <Provider store={store}>
          <StaticRouter location={pathname} context={context}>
            <div>{renderRoutes(Routes)}</div>
          </StaticRouter>
        </Provider>
      </Loadable.Capture>
  );

    console.log("modules",modules)
    let bundles = getBundles(stats, modules);

  return `
  <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>GiftTek</title>
          <link rel="shortcut icon" href="/style/favicon_gift.png">
          <link rel="stylesheet" type="text/css" href="/style/style.css"/>
          <link rel="stylesheet" type="text/css" href="/style/slik/slick.css"/>
            <link rel="stylesheet" type="text/css" href="/style/slik/slick-theme.css"/>
            
      </head>
      <body>
      <script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="10460531-af76-474b-8867-9ada00dd41dc";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>
      <div id="root">${content}</div>
      <script>
        window.INITIAL_STATE = ${JSON.stringify(store.getState())}
      </script>
      
      <script src="/dist/vendor.js"></script>
        ${bundles.map(bundle => { return `<script src="/dist/${bundle.file}"></script>`}).join('\\n')}
       <script src="/dist/main.js"></script>
      
      </body>
      </html>
  `;
};